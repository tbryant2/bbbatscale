#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'BBBatScale.settings.development')

    cov = None
    if 'test' in sys.argv[1:]:
        from coverage import Coverage
        cov = Coverage()
        cov.erase()
        cov.start()

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)

    if cov is not None:
        cov.stop()
        cov.save()
        cov.report()
        cov.xml_report()


if __name__ == '__main__':
    main()

from math import ceil

from django.db import transaction

from django.db import models
from django.db.models import Sum
from django.utils.crypto import get_random_string
from django.utils.timezone import now

from core.utils import BigBlueButton

import logging

from core.constants import INSTANCE_STATES, INSTANCE_STATE_WAITING_RESPONSE, INSTANCE_STATE_ERROR, \
    SCHEDULING_STRATEGY_LEAST_PARTICIPANTS, INSTANCE_STATE_UP, INSTANCE_STATE_DISABLED, \
    SCHEDULING_STRATEGY_LEAST_UTILIZATION, GUEST_POLICY, GUEST_POLICY_ALLOW, ROOM_STATE, ROOM_STATE_INACTIVE, \
    SCHEDULING_STRATEGIES, ROOM_STATE_ACTIVE

logger = logging.getLogger(__name__)


class Tenant(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    notifications_emails = models.TextField(null=True, blank=True)
    token_registration = models.CharField(max_length=255, null=True, blank=True)
    scheduling_strategy = models.CharField(max_length=255, choices=SCHEDULING_STRATEGIES,
                                           default=SCHEDULING_STRATEGY_LEAST_UTILIZATION)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.token_registration:
            self.token_registration = get_random_string(length=10)
        super(Tenant, self).save(*args, **kwargs)

    def get_current_participant_count(self):
        return self.room_set.all().aggregate(Sum('participant_count'))['participant_count__sum']

    def get_utilization(self):
        usage = 0
        for instance in self.get_instances_up():
            usage = usage + instance.get_utilization()
        return ceil(usage)

    def get_utilization_max(self):
        sum = 0
        for instance in self.get_instances_up():
            sum = sum + instance.videostream_count_max
        return ceil(sum)

    def get_utilization_percent(self):
        max = self.get_utilization_max()
        return 0 if max <= 0 else int(round((self.get_utilization()) / self.get_utilization_max(), 2) * 100)

    def get_instance_for_room(self):
        instance_to_be = None

        logger.info("Selecting new instance for scheduling with strategy: {}".format(self.scheduling_strategy))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_PARTICIPANTS:
            min_count = 1000
            for instance in self.instance_set.filter(state=INSTANCE_STATE_UP):
                pc = instance.get_participant_count()
                if pc < min_count:
                    min_count = pc
                    instance_to_be = instance
                    logger.info("Best candidate so far: {}".format(instance_to_be))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_UTILIZATION:
            min_count = 1000
            for instance in self.instance_set.filter(state=INSTANCE_STATE_UP):
                pc = instance.get_utilization()
                if pc < min_count:
                    min_count = pc
                    instance_to_be = instance
                    logger.info("Best candidate so far: {}".format(instance_to_be))

        logger.info("Selected : {}".format(instance_to_be))

        return instance_to_be

    def get_instances_up(self):
        return self.instance_set.filter(state=INSTANCE_STATE_UP)


class RoomConfiguration(models.Model):
    name = models.CharField(max_length=255, unique=True)
    mute_on_start = models.BooleanField(default=True)
    all_moderator = models.BooleanField(default=False)
    everyone_can_start = models.BooleanField(default=False)
    authenticated_user_can_start = models.BooleanField(default=False)
    guest_policy = models.CharField(max_length=30, choices=GUEST_POLICY, default=GUEST_POLICY_ALLOW, blank=True)
    allow_guest_entry = models.BooleanField(default=False)
    access_code = models.CharField(max_length=255, null=True, blank=True)
    access_code_guests = models.CharField(max_length=255, null=True, blank=True)
    disable_cam = models.BooleanField(default=False)
    disable_mic = models.BooleanField(default=False)
    allow_recording = models.BooleanField(default=False)
    disable_private_chat = models.BooleanField(default=False)
    disable_public_chat = models.BooleanField(default=False)
    disable_note = models.BooleanField(default=False)
    url = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Instance(models.Model):
    tenant = models.ForeignKey(Tenant, on_delete=models.SET_NULL, null=True)
    dns = models.CharField(max_length=255, unique=True)
    state = models.CharField(max_length=255, choices=INSTANCE_STATES, default=INSTANCE_STATE_WAITING_RESPONSE)
    datacenter = models.CharField(max_length=255, null=True, blank=True)
    shared_secret = models.CharField(max_length=255, null=True, blank=True)
    participant_count_max = models.PositiveIntegerField(default=250)
    videostream_count_max = models.PositiveIntegerField(default=25)

    last_health_check = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ('tenant', 'dns',)

    def __str__(self):
        return self.dns

    # quick and dirty util cals.
    def get_utilization(self):
        audio_video_factor = self.participant_count_max / self.videostream_count_max
        audio_usage = self.get_participant_count() / audio_video_factor
        video_usage = self.get_videostream_count()
        total_usage = audio_usage + video_usage

        return 0 if total_usage <= 0 else total_usage

    def get_utilization_percent(self):
        return int(round((self.get_utilization()) / self.videostream_count_max, 2) * 100)

    # todo change to service.py
    def collect_stats(self):
        logger.info("Health Check started for.... {}".format(self.dns))
        # skip disabled instances
        if self.state != INSTANCE_STATE_DISABLED:
            try:
                bbb = BigBlueButton(self.dns, self.shared_secret)
                meetings = bbb.get_meetings()

                if meetings:
                    for meeting in meetings:
                        logger.info("Starting transaction for updating room {}".format(meeting['meetingName']))
                        with transaction.atomic():
                            room = Room.objects.get_or_create(name=meeting['meetingName'])[0]
                            room.instance = self
                            room.state = ROOM_STATE_ACTIVE
                            room.tenant = self.tenant
                            room.participant_count = meeting['participantCount']
                            room.videostream_count = meeting['videoCount']
                            room.is_breakout = True if meeting['isBreakout'] == 'true' else False
                            room.last_running = now()
                            room.save()
                        logger.info("Ending transaction for updating room {}".format(meeting['meetingName']))

                self.state = INSTANCE_STATE_UP

            except Exception as e:
                logger.error(e)
                logger.error("setting instance state of {} to ERROR".format(self.dns))
                self.state = INSTANCE_STATE_ERROR

            self.last_health_check = now()
            self.save()
        logger.info("Health Check finished for.... {}".format(self.dns))

    def get_participant_count(self):
        count = self.room_set.all().aggregate(Sum('participant_count'))['participant_count__sum']
        return count if count else 0

    def get_videostream_count(self):
        count = self.room_set.all().aggregate(Sum('videostream_count'))['videostream_count__sum']
        return count if count else 0


class Room(models.Model):
    tenant = models.ForeignKey(Tenant, on_delete=models.SET_NULL, null=True)
    instance = models.ForeignKey(Instance, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=255, unique=True, null=True)
    config = models.ForeignKey(RoomConfiguration, on_delete=models.SET_NULL, blank=True, null=True)
    meeting_id = models.CharField(max_length=255, null=True, blank=True, unique=True)
    is_public = models.BooleanField(default=True)
    state = models.CharField(max_length=30, choices=ROOM_STATE, default=ROOM_STATE_INACTIVE)

    # Room Config
    mute_on_start = models.BooleanField(default=True)
    all_moderator = models.BooleanField(default=False)
    everyone_can_start = models.BooleanField(default=False)
    authenticated_user_can_start = models.BooleanField(default=False)
    guest_policy = models.CharField(max_length=30, choices=GUEST_POLICY, default=GUEST_POLICY_ALLOW, blank=True)
    allow_guest_entry = models.BooleanField(default=False)
    access_code = models.CharField(max_length=255, null=True, blank=True)
    access_code_guests = models.CharField(max_length=255, null=True, blank=True)
    disable_cam = models.BooleanField(default=False)
    disable_mic = models.BooleanField(default=False)
    allow_recording = models.BooleanField(default=False)
    disable_private_chat = models.BooleanField(default=False)
    disable_public_chat = models.BooleanField(default=False)
    disable_note = models.BooleanField(default=False)
    url = models.CharField(max_length=255, null=True, blank=True)

    attendee_pw = models.CharField(max_length=255, null=True, blank=True)
    moderator_pw = models.CharField(max_length=255, null=True, blank=True)

    comment_public = models.CharField(max_length=255, null=True, blank=True)
    comment_private = models.CharField(max_length=255, null=True, blank=True)

    click_counter = models.PositiveIntegerField(default=0)

    participant_count = models.PositiveIntegerField(default=0)
    videostream_count = models.PositiveIntegerField(default=0)

    is_breakout = models.BooleanField(default=False)

    event_collection_strategy = models.CharField(max_length=255, null=True, blank=True)
    event_collection_parameters = models.TextField(null=True, blank=True)

    last_running = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    @staticmethod
    def get_participants_current():
        return Room.objects.all().aggregate(Sum('participant_count'))['participant_count__sum']

    @staticmethod
    def get_total_instantiation_counter():
        return Room.objects.all().aggregate(Sum('instantiation_counter'))['instantiation_counter__sum']

    def is_meeting_running(self):
        bbb = BigBlueButton(self.instance.dns, self.instance.shared_secret)
        return bbb.is_meeting_running(self.meeting_id)

    def get_meeting_infos(self):
        bbb = BigBlueButton(self.instance.dns, self.instance.shared_secret)
        bbb.get_meeting_infos(self.meeting_id)

    def end_meeting(self):
        bbb = BigBlueButton(self.instance.dns, self.instance.shared_secret)
        return bbb.end(self.meeting_id, self.moderator_pw)


class RoomEvent(models.Model):
    uid = models.CharField(max_length=255, primary_key=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=True)
    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return "{} – {}".format(self.room, self.name)

    class Meta:
        ordering = ('room',)


class GeneralParameter(models.Model):
    latest_news = models.TextField(null=True, blank=True)
    participant_total_max = models.PositiveIntegerField(default=0)
    jitsi_url = models.CharField(max_length=255, blank=True, null=True, default="https://meet.jit.si/")
    playback_url = models.CharField(max_length=255, blank=True, null=True)
    faq_url = models.CharField(max_length=255, blank=True, null=True)
    feedback_email = models.CharField(max_length=255, blank=True, null=True)
    footer = models.TextField(blank=True, null=True)
    logo_link = models.CharField(max_length=510, blank=True, null=True)
    favicon_link = models.CharField(max_length=510, blank=True, null=True)
    page_title = models.CharField(max_length=510, blank=True, null=True, default="bbb@scale | Virtual Rooms")
    support_chat_link = models.TextField(blank=True, null=True)


class Meeting(models.Model):
    room_name = models.CharField(max_length=255, null=True, blank=True)
    creator = models.CharField(max_length=255, null=True, blank=True)
    started = models.DateTimeField(auto_now_add=True)
    obs_occupancy = models.CharField(max_length=255, null=True, blank=True)
    replay_id = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "{}-{}-{}".format(self.started.date(), self.room_name, self.creator)

    class Meta:
        ordering = ('-started',)
